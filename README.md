# File Daemon

It scans and monitors directories to sync their metadata to a database. It also records all processes and the opened files.
It supports Mac OS and Linux. 

## Install

It requires Python 3.3+.

```bash
pip install -r requirements.txt
```

## Run

```bash
python monitor.py <directory to montior> <direcotry to monitor> ...
```

When you run it, it will store the metadata of all the files under the monitored directories to sqlite.
After that, it will continue to monitor the changes of the directories and make sure the metadata in the database is consistent with the files in the directories.

### Examples
Monitor the currently directory.

```bash
python monitor.py .
```

Monitor /tmp and /home

```bash
python monitor.py /bin /home
```

## Results

It has two tables in a sqlite database. 

![The Tables](screenshots/tables.png)

### Table 'file'

The table 'file' contains all the files and directories in the specified root directory, 
which are collected by traversing the root directory, 
and monitored to maintain a consistent metadata of files under the root directory.


![The Files and Directories](screenshots/file.png)

### Table 'opened_files'

The results are got from the command line 'lsof', available on Mac and Linux. It has all processes and the files opened.

![The Opened Files and Processes](screenshots/opened_files.png)

## TODO

The project is pretty hacky. 
For example, the database access really needs to be written in a non-blocking io way.
The processes and opened files are got by calling the command line 'lsof'. It should be properly programmed.
And it doesn't support windows.
