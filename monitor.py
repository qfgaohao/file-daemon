import sqlite3
import os
import sys
import logging
from collections import namedtuple
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
import time
import subprocess

import config

Meta = namedtuple('Meta', ['path', 'name', 'type', 'access_time', 'modification_time', 'size'])


def lsof(db_path):
    """List all the opened files.

    TODO: NEEDS A BETTE WAY THAN USING A COMMAND LINE.
    """
    r = subprocess.run(['lsof'], stdout=subprocess.PIPE)
    r = r.stdout.decode('utf-8')
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()
    for i, line in enumerate(r.split("\n")):
        if i == 0:
            headers = [e.strip().replace("/", "_") + " text" for e in line.rstrip().split()]
            header_str = "(" + ", ".join(headers) + ")"
            sql = f'''CREATE TABLE opened_files {header_str}'''
            logging.info(sql)
            cursor.execute(sql)
        else:
            t = [e for e in line.rstrip().split()]
            if not t or not (t[4].lower() == "dir" or t[4].lower() == "reg"):
                continue
            if len(t) > len(headers):
                t[len(headers) - 1] = " ".join(t[len(headers) - 1:])
                t = t[:len(headers)]
            t = [f"""'{e.replace("'", "''")}'""" for e in t]
            sql = f'''INSERT INTO opened_files VALUES ({",".join(t)})'''
            logging.info(sql)
            cursor.execute(sql)
    logging.info(f"{i} files are open right now.")
    connection.commit()


def get_suffix(p):
    name = os.path.basename(p)
    t = name.split(".")
    suffix = t[-1]
    return suffix.lower()


def create_table(connection, cursor, table):
    cursor.execute(f'''CREATE TABLE {table}
                 (path text, name text, type text, creation_time real, modification_time real, size integer)''')
    connection.commit()


def insert_to_table(row, connection, cursor, table, commit=True):
    cursor.execute(f"INSERT INTO {table} VALUES {tuple(row)}")
    if commit:
        connection.commit()


def delete_from_table(path, connection, cursor, table, commit=True):
    cursor.execute(f'DELETE FROM {table} where path="{path}"')
    if commit:
        connection.commit()


def create_file_meta(p):
    try:
        info = os.lstat(p)
    except PermissionError as e:
        logging.warn(f"Have no permeission for file:{p}.")
        return Meta(p, os.path.basename(p), "PermissionError", 0, 0, 0)
    file_type = get_suffix(p)
    name = os.path.basename(p)
    return Meta(p, name, file_type, info.st_atime, info.st_mtime, info.st_size)


def create_dir_meta(p):
    try:
        info = os.lstat(p)
    except PermissionError as e:
        logging.warning(f"Have no permeission for directory:{p}.")
        return Meta(p, os.path.basename(p), "PermissionError", 0, 0, 0)
    name = os.path.basename(p)
    return Meta(p, name, "DIR", info.st_atime, info.st_mtime, info.st_size)


def scan(folder, connection, cursor, table):
    num_file = 0
    num_dir = 0
    for root, dirs, files in os.walk(folder):
        for dir in dirs:
            p = os.path.join(root, dir)
            meta = create_dir_meta(p)
            logging.info(f"Found directory {p} with metadata {meta}.")
            insert_to_table(meta, connection, cursor, table, False)
            num_dir += 1
        for file in files:
            p = os.path.join(root, file)
            meta = create_file_meta(p)
            logging.info(f"Found file {p} with metadata {meta}.")
            insert_to_table(meta, connection, cursor, table, False)
            num_file += 1
    connection.commit()
    return num_dir, num_file


class EventHandler (FileSystemEventHandler):
    def __init__(self, root, db_path, table):
        super(FileSystemEventHandler, self).__init__()
        self.db_path = db_path
        self.table = table
        self.root = root

    def on_created(self, event):
        connection = sqlite3.connect(self.db_path)
        cursor = connection.cursor()
        if event.is_directory:
            p = event.src_path
            meta = create_dir_meta(p)
            logging.info(f"Created new directory: {p}.")
            insert_to_table(meta, connection, cursor, self.table, True)
        else:
            p = event.src_path
            meta = create_file_meta(p)
            logging.info(f"Created new file: {p}.")
            insert_to_table(meta, connection, cursor, self.table, True)
        connection.close()

    def on_deleted(self, event):
        connection = sqlite3.connect(self.db_path)
        cursor = connection.cursor()
        p = event.src_path
        logging.info(f"{p} has been deleted.")
        delete_from_table(p, connection, cursor, self.table, True)
        connection.close()

    def on_modified(self, event):
        if not event.is_directory:
            if event.src_path == config.db_path:
                return
            connection = sqlite3.connect(self.db_path)
            cursor = connection.cursor()
            p = event.src_path
            logging.info(f"File modified: {p}.")
            delete_from_table(p, connection, cursor, self.table, True)
            meta = create_file_meta(p)
            insert_to_table(meta, connection, cursor, self.table, True)
            connection.close()

    def on_moved(self, event):
        connection = sqlite3.connect(self.db_path)
        cursor = connection.cursor()
        logging.info(f"{event.src_path} has been moved to {event.dest_path}.")
        delete_from_table(event.src_path, connection, cursor, self.table, True)
        if event.dest_path.startswith(self.root):
            delete_from_table(event.dest_path, connection, cursor, self.table, True)
            meta = create_file_meta(event.dest_path)
            insert_to_table(meta, connection, cursor, self.table, True)
        connection.close()


def monitor(path, db_path, table):
    event_handler = EventHandler(path, db_path, table)
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    return observer


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    if len(sys.argv) < 2:
        logging.fatal("Usage python3 <directory to monitor> <directory to monitor> ...")
        sys.exit(1)

    if os.path.exists(config.db_path):
        logging.info(f"Removing the existing db file {config.db_path}")
        os.remove(config.db_path)

    logging.info("Get all opened files. It will take a few minutes.")
    lsof(config.db_path)

    logging.info("Check the folders you want to monitor.")
    folders = sys.argv[1:]
    for folder in folders:
        if not os.path.exists(folder):
            logging.fatal(f"{folder} doesn't exist.")
            sys.exit(1)
        if not os.path.isdir(folder):
            logging.fatal(f"{folder} is not a directory.")
            sys.exit(1)
    folders = set([os.path.abspath(d) for d in folders])

    connection = sqlite3.connect(config.db_path)
    cursor = connection.cursor()

    logging.info(f"Create table {config.table_name} for file metadata in database {config.db_path}.")
    create_table(connection, cursor, config.table_name)

    for folder in folders:
        logging.info(f"Do an initial scan for directory {folder}.")
        num_dir, num_file = scan(folder, connection, cursor, config.table_name)
        logging.info(f"Found {num_dir} directories and {num_file} files.")
    observers = []
    for folder in folders:
        logging.info(f"Start monitoring directory {folder}")
        observer = monitor(folder, config.db_path, config.table_name)
        observers.append(observer)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for observer in observers:
            observer.stop()

    for observer in observers:
        observer.join()
